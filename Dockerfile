FROM gledson/python3.6.1-slim:latest

RUN apt-get update && apt-get install vim -y

RUN mkdir -p /usr/share/workdir
COPY ./ /usr/share/workdir

WORKDIR /usr/share/workdir

RUN apt-get update -y && apt-get install wget -y && pip install --upgrade pip && pip install -r requirements.txt

RUN apt-get install -y sqlite3 libsqlite3-dev

RUN wget -qO- https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/5.2.4/flyway-commandline-5.2.4-linux-x64.tar.gz | tar xvz && ln -s `pwd`/flyway-5.2.4/flyway /usr/local/bin

RUN flyway -user= -password= -url=jdbc:sqlite:src/dao/layout_final.db3 -locations=filesystem:src/dao/migration -baselineOnMigrate=true -baselineVersion=0 migrate

CMD [ "python", "./run.py" ]
-- SQLite
--Verificar o total de faturas no arquivo custom:
SELECT 
	COUNT(*) AS [Total de Faturas]
FROM detalhe_10;



--Para verificar o total de faturas extratadas e não extratadas:
SELECT 
	COUNT(d10.id_operacao) as [Total de Faturas], 
	d.emite_extrato AS [Emite Extrato], 
	d10.vencimento_da_fatura_1 AS [Vencimento de Fatura]
FROM detalhe_10 AS d10
INNER JOIN detalhe_fatura_12 d ON d10.id_operacao = d.id_operacao
GROUP BY d.emite_extrato, d10.vencimento_da_fatura_1;



--Extrair o total de linhas digitáveis:
SELECT 
	COUNT(d12.id_operacao) AS [Total de Linhas Digitaveis]
FROM detalhe_fatura_12 d12
INNER JOIN detalhe_10 d10 ON d12.id_operacao = d10.id_operacao
WHERE d12.emite_extrato = 1
	AND d12.linha_digitavel <> 'Null';



--Total de Faturas com Saldo Negativo:
SELECT 
	COUNT(d10.id_operacao) AS [Total de Faturas Emitidas com Saldo Negativo]
FROM detalhe_fatura_12 d12
INNER JOIN detalhe_10 d10 ON d12.id_operacao = d10.id_operacao
WHERE d12.emite_extrato = 1 
	AND d12.saldo_atual_do_extrato < 0;



-- Total de Faturas que irao para o SF
SELECT 
	COUNT(d10.id_operacao) AS [Total de Faturas Emitidas que irao para o SF]
FROM detalhe_fatura_12 d12
INNER JOIN detalhe_10 d10 ON d12.id_operacao = d10.id_operacao
INNER JOIN detalhe_credenciado_13 d13 ON d13.id_operacao = d10.id_operacao
WHERE d12.emite_extrato = 1 
	AND d12.saldo_atual_do_extrato >= 6;



--Total de Faturas com Saldo Positivo:
SELECT 
	COUNT(d10.id_operacao) AS [Total de Faturas Emitidas com Saldo Positivo]
FROM detalhe_fatura_12 d12
INNER JOIN detalhe_10 d10 ON d12.id_operacao = d10.id_operacao
WHERE d12.emite_extrato = 1 
	AND d12.saldo_atual_do_extrato > 0;



--Consulta gera o relatório que deve ser repassado ao setor de faturas para realização de validação. 
SELECT
	SUBSTR(d10.codigo_de_barras_1,2,9) as id_conta,
	d10.nome_do_cliente,
	d12.saldo_atual_do_extrato,
	d12.emite_extrato,
	d10.recebimento_fatura,
	d10.numero_do_nosso_numero,
	d24.nome_da_campanha,
	GROUP_CONCAT(d25.quantidade_de_parcelas) as opcoes_parcelamento,
	GROUP_CONCAT(d25.valor_da_parcela) as valor_parcela,
	GROUP_CONCAT(d25.valor_total_do_refinanciamento) as valor_total_do_refinanciamento,
	d25.taxa_juros as taxa_juros,
	d12.linha_digitavel
FROM detalhe_10 d10
INNER JOIN detalhe_fatura_12 d12 ON d12.id_operacao = d10.id_operacao
LEFT JOIN detalhe_24 d24 ON d24.id_operacao = d10.id_operacao
LEFT JOIN detalhe_25 d25 ON d25.id_operacao = d10.id_operacao
GROUP BY d10.id_operacao;

SELECT
	SUBSTR(d10.codigo_de_barras_1,2,9) as id_conta,
	d10.nome_do_cliente,
	d12.saldo_atual_do_extrato,
	d12.emite_extrato,
	d10.recebimento_fatura,
	d10.numero_do_nosso_numero,
	d24.nome_da_campanha,
	GROUP_CONCAT(d25.quantidade_de_parcelas) as opcoes_parcelamento,
	GROUP_CONCAT(d25.valor_da_parcela) as valor_parcela,
	GROUP_CONCAT(d25.valor_total_do_refinanciamento) as valor_total_do_refinanciamento,
	d25.taxa_juros as taxa_juros,
	d12.linha_digitavel
FROM detalhe_10 d10
INNER JOIN detalhe_fatura_12 d12 ON d12.id_operacao = d10.id_operacao
LEFT JOIN detalhe_24 d24 ON d24.id_operacao = d10.id_operacao
LEFT JOIN detalhe_25 d25 ON d25.id_operacao = d10.id_operacao
WHERE d12.emite_extrato = 1 
GROUP BY d10.id_operacao;




SELECT
	SUBSTR(d10.codigo_de_barras_1,2,9) as id_conta,
	d10.nome_do_cliente,
	d12.saldo_atual_do_extrato,
	d12.emite_extrato,
	d10.recebimento_fatura,
	d10.numero_do_nosso_numero,
	d24.nome_da_campanha,
	GROUP_CONCAT(d25.quantidade_de_parcelas) as opcoes_parcelamento,
	GROUP_CONCAT(d25.valor_da_parcela) as valor_parcela,
	GROUP_CONCAT(d25.valor_total_do_refinanciamento) as valor_total_do_refinanciamento,
	d25.taxa_juros as taxa_juros,
	d12.linha_digitavel
FROM detalhe_10 d10
INNER JOIN detalhe_fatura_12 d12 ON d12.id_operacao = d10.id_operacao
LEFT JOIN detalhe_24 d24 ON d24.id_operacao = d10.id_operacao
LEFT JOIN detalhe_25 d25 ON d25.id_operacao = d10.id_operacao
GROUP BY d10.id_operacao;



-- CONSULTA DE CONTAS 
/*
SELECT
	SUBSTR(d10.codigo_de_barras_1,2,9) as id_conta,
	d10.nome_do_cliente,
	d12.saldo_atual_do_extrato,
	d12.emite_extrato,
	d10.recebimento_fatura,
	d24.nome_da_campanha,
	GROUP_CONCAT(d25.valor_da_parcela) as valor_parcela,
	GROUP_CONCAT(d25.valor_total_do_refinanciamento) as valor_total_do_refinanciamento,
	d25.taxa_juros as taxa_juros,
	d12.linha_digitavel
FROM detalhe_10 d10
INNER JOIN detalhe_fatura_12 d12 ON d12.id_operacao = d10.id_operacao
LEFT JOIN detalhe_24 d24 ON d24.id_operacao = d10.id_operacao
LEFT JOIN detalhe_25 d25 ON d25.id_operacao = d10.id_operacao
WHERE d12.emite_extrato = 1 
AND CAST(SUBSTR(d10.codigo_de_barras_1,2,9) AS INT) IN (264,445,911,2520,3629)
*/


--Consulta gera relatório das faturas que serão emitidas, mas que seus valores forem menores que 0 (negativos)
/*
SELECT
	SUBSTR(d10.codigo_de_barras_1,2,9) as id_conta,
	d10.nome_do_cliente,
	d12.saldo_atual_do_extrato,
	d12.emite_extrato,
	d10.recebimento_fatura,
	d10.numero_do_nosso_numero,
	d24.nome_da_campanha,
	GROUP_CONCAT(d25.quantidade_de_parcelas) as opcoes_parcelamento,
	GROUP_CONCAT(d25.valor_da_parcela) as valor_parcela,
	GROUP_CONCAT(d25.valor_total_do_refinanciamento) as valor_total_do_refinanciamento,
	d25.taxa_juros as taxa_juros,
	d12.linha_digitavel
FROM detalhe_10 d10
INNER JOIN detalhe_fatura_12 d12 ON d12.id_operacao = d10.id_operacao
LEFT JOIN detalhe_24 d24 ON d24.id_operacao = d10.id_operacao
LEFT JOIN detalhe_25 d25 ON d25.id_operacao = d10.id_operacao
WHERE d12.emite_extrato = 1 
	AND d12.saldo_atual_do_extrato < 0 
GROUP BY d10.id_operacao;



--Consulta gera o relatório que deve ser repassado ao setor de faturas para realização de validação. 
SELECT
	SUBSTR(d10.codigo_de_barras_1,2,9) as id_conta,
	d10.nome_do_cliente,
	d12.saldo_atual_do_extrato,
	d12.emite_extrato,
	d10.recebimento_fatura,
	d10.numero_do_nosso_numero,
	d24.nome_da_campanha,
	GROUP_CONCAT(d25.quantidade_de_parcelas) as opcoes_parcelamento,
	GROUP_CONCAT(d25.valor_da_parcela) as valor_parcela,
	GROUP_CONCAT(d25.valor_total_do_refinanciamento) as valor_total_do_refinanciamento,
	d25.taxa_juros as taxa_juros,
	d12.linha_digitavel
FROM detalhe_10 d10
INNER JOIN detalhe_fatura_12 d12 ON d12.id_operacao = d10.id_operacao
LEFT JOIN detalhe_24 d24 ON d24.id_operacao = d10.id_operacao
LEFT JOIN detalhe_25 d25 ON d25.id_operacao = d10.id_operacao
WHERE d12.emite_extrato = 1 
	AND d12.saldo_atual_do_extrato >= 6 
GROUP BY d10.id_operacao;
*/
# encoding: utf-8
from peewee import sqlite3
from src.utils.TmpFile import TmpFile
from datetime import datetime


class ScriptSql(object):
    tmp_file = TmpFile()
    print('Inicializando o script SQL')

    def __init__(self):
        print('Conectando arquivo SQLLite src/dao/layout_final.db3')
        self.conn = sqlite3.connect('src/dao/layout_final.db3')
        self.cursor = self.conn.cursor()
        print('Conexao efetuada com sucesso')

    def execute_arquivo(self, file):

        #alteracao efetuada para gravar toda a instrucao para um comando unico de SQL
        print('Gravando arquivo de scripts SQL')
        fileScript = 'arquivos_sql_insert_bulk/sqlScriptInsert_'+ datetime.now().strftime("%Y-%m-%d_%H-%M-%S") +'.sql'
        fileWrite = open(fileScript, "w", encoding="ISO-8859-1")
        fileWrite.writelines(file)
        fileWrite.close()
        print('Finalização da gravação do arquivo de scripts SQL')


        print('Execução de querys SQL')
        qnt_interacao = 250
        qnt_corte = int(len(file) / qnt_interacao)
        qnt_atual = 0
        for sql in file:
            try:
                print('Executando arquivo SQL')
                sql_ajustado = sql.replace('None', 'Null')
                self.cursor.execute(sql_ajustado)
            except Exception as error_msg:
                self.insert_log_error(error_msg.args[0], sql_ajustado)
            qnt_atual += 1
            if qnt_atual > qnt_corte:
                qnt_atual = 0
                self.conn.commit()
                qnt_interacao = qnt_interacao - 1
                print(qnt_interacao)

        self.conn.commit()
        print('Execução de scripts SQL finalizado')


    def execute(self, sql):
        try:
            self.tmp_file.set_data_sql(sql.replace('\n', ''))
        except Exception as error:
            self.insert_log_error_and_commit(error, sql)

    def execute_and_return(self, sql):
        try:
            self.cursor.execute(sql)
            return self.cursor.fetchall()
        except Exception as error:
            self.insert_log_error_and_commit(error, sql)

    def insert_log_error(self, error_msg, sql):
        sql = "INSERT INTO log_error VALUES('{}', '{}');".format(error_msg, sql.replace("'", ''))
        self.cursor.execute(sql)

    def insert_log_error_and_commit(self, error_msg, sql):
        sql = "INSERT INTO log_error VALUES('{}', '{}');".format(error_msg, sql.replace("'", ''))
        self.cursor.execute(sql)
        self.conn.commit()

    def get_log(self):
        sql = "SELECT error_msg, COUNT(*) AS qnt FROM log_error GROUP BY error_msg ORDER BY qnt DESC;"
        return self.execute_and_return(sql)

    def insert_header_fatura(self, dados):
        sql = "INSERT INTO header_00 (numero_da_linha_1, nome_do_cedente, fixo_0_1, agencia, conta_corrente, data_do_documento, data_do_processamento, fixo_0_2, filler1) " \
              "VALUES ({}, '{}', {}, {}, {}, {}, {}, {}, '{}');".format(int(dados['numero_da_linha_1']), dados['nome_do_cedente'], int(dados['fixo_0_1']), int(dados['agencia']), int(dados['conta_corrente']), dados['data_do_documento'], dados['data_do_processamento'], int(dados['fixo_0_2']), dados['filler_1'])
        self.execute(sql)

    def insert_detalhe_10(self, dados):
        sql = "INSERT INTO detalhe_10 (numero_da_linha_2, numero_do_cartao_do_cliente, nome_do_cliente, endereco_do_cliente, numero_do_endereco, complemento_do_endereco, bairro_do_cliente, cidade_do_cliente, unidade_federativa_do_cliente, cep_do_cliente, numero_do_documento_do_cliente, numero_do_nosso_numero, especie_do_documento, aceite, brancos_1, numero_da_carteira, brancos_2, fixo_0, vencimento_da_fatura_1, descricoes_da_fatura_2, descricoes_da_fatura_3, descricoes_da_fatura_4, descricoes_da_fatura_5, codigo_de_barras_1, deficiente, codigo_cedente_do_cliente, recebimento_fatura, email, filler_2, id_operacao) " \
              "VALUES ({},'{}','{}','{}','{}','{}','{}','{}','{}',{},{},{},'{}','{}',{},{},{},{},{},'{}','{}','{}','{}',{},{},{},'{}','{}',{},{});"\
            .format(dados['numero_da_linha_2'], dados['numero_do_cartao_do_cliente'], dados['nome_do_cliente'], dados['endereco_do_cliente_1'], dados['numero_do_endereco'], dados['complemento_do_endereco'], dados['bairro_do_cliente'], dados['cidade_do_cliente'], dados['unidade_federativa_do_cliente'], dados['cep_do_cliente'], dados['numero_do_documento_do_cliente'], dados['numero_do_nosso_numero'], dados['especie_do_documento'], dados['aceite'], dados['brancos_1'], dados['numero_da_carteira'], dados['brancos_2'], dados['fixo_0'], dados['vencimento_da_fatura'], dados['descricoes_da_fatura_1'], dados['descricoes_da_fatura_2'], dados['descricoes_da_fatura_3'], dados['descricoes_da_fatura_4'], dados['codigo_de_barras_1'], dados['deficiente'], dados['codigo_cedente_do_cliente'], dados['recebmento_fatura'], dados['email'], dados['filler'], dados['id_operacao'])
        self.execute(sql)

    def insert_detalhe_11(self, dados):
        sql = "INSERT INTO detalhe_11 " \
                "VALUES ({}, {}, {}, {}, {}, '{}', {}, '{}', {}, {}, {}, {}, '{}', {}, {}, {}, {}, {}, '{}', {});".format(dados['numero_da_linha_3'], dados['limite_maximo_para_compras_nacional_1'], dados['limite_maximo_para_saque_nacional_2'], dados['limite_maximo_para_compras_nacional_3'], dados['valores_de_encargos_financeiros'], dados['juros_1'], dados['valores_de_encargos_de_atraso_1'], dados['juros_2'], dados['limite_maximo_para_parcelado'], dados['limite_maximo_para_compras'], dados['valores_de_encargos_de_atraso_2'], dados['valor_do_custo_efetivo_total'], dados['juros_3'], dados['data_do_fechamento_da_proxima_fatura'], dados['total_saldo_parcelado_pendente_de_liquidacao'], dados['total_credito_parcelado'], dados['total_credito_parcela_parcelado'], dados['encargos_para_o_proximo_mes_no_caso_de_pagamento_minimo_da_fatura'], dados['filler_3'], dados['id_operacao'])
        self.execute(sql)

    def insert_detalhe_fatura_12(self, dados):
        sql = "INSERT INTO detalhe_fatura_12 " \
                "VALUES ({}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, '{}', '{}', {}, '{}', {}, '{}', {});".format(dados['numero_da_linha_4'], dados['saldo_anterior_do_extrato'], dados['creditos_nacionais'], dados['debitos_nacionais'], dados['saldo_atual_do_extrato'], dados['valor_minimo_do_extrato'], dados['pagamentos'], dados['valor_das_compras_nacionais'], dados['encargos_de_financiamento'], dados['acertos_diversos'], dados['quantidade_de_dias_em_atraso_1'], dados['pontuacao_total_no_ciclo'], dados['pontuacao_usada_no_ciclo'], dados['pontos_remanescentes'], dados['senha'], dados['aniversariante_do_mes'], dados['codigo_de_barras_2'], dados['linha_digitavel'], dados['emite_extrato'], dados['filler_4'], dados['id_operacao'])
        self.execute(sql)

    def insert_detalhe_fatura_12_nao_extratavel(self, dados):
        sql = "INSERT INTO detalhe_fatura_12 " \
                "VALUES ({}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, '{}', '{}', {}, '{}', {}, '{}', {});".format(dados['numero_da_linha_4'], dados['saldo_anterior_do_extrato'], dados['creditos_nacionais'], dados['debitos_nacionais'], dados['saldo_atual_do_extrato'], dados['valor_minimo_do_extrato'], dados['pagamentos'], dados['valor_das_compras_nacionais'], dados['encargos_de_financiamento'], dados['acertos_diversos'], dados['quantidade_de_dias_em_atraso_1'], dados['pontuacao_total_no_ciclo'], dados['pontuacao_usada_no_ciclo'], dados['pontos_remanescentes'], dados['senha'], dados['aniversariante_do_mes'], dados['codigo_de_barras_2'], dados['linha_digitavel'], dados['emite_extrato'], dados['filler_4'], dados['id_operacao'])
        self.execute(sql)

    def insert_detalhe_credenciado_13(self, dados):
        sql = "INSERT INTO detalhe_credenciado_13 " \
                "VALUES ({}, {}, {}, '{}', {}, {}, {}, '{}', '{}', {});".format(dados['numero_da_linha_5'], dados['codigo_da_origem_comercial'], dados['cnpj_do_estabelecimento'], dados['nome_fantasia_do_lojista'], dados['codigo_da_fantasia_basica'], dados['quantidade_de_dias_em_atraso_2'], dados['codigo_do_produto'], dados['descricao_do_produto'], dados['filler_5'], dados['id_operacao'])
        self.execute(sql)

    def insert_detalhe_detalhe_fatura_fisica_20(self, dados):
        sql = "INSERT INTO detalhe_fatura_fisica_20 " \
                "VALUES ({}, '{}', '{}', {});".format(dados['numero_da_linha_6'], dados['nome_do_plastico_do_cartao'], dados['filler_6'], dados['id_operacao'])
        self.execute(sql)

    def insert_detalhe_21(self, dados):
        sql = "INSERT INTO detalhe_21 " \
                "VALUES ({}, {}, '{}', '{}', '{}', {}, {}, '{}', {});".format(dados['numero_da_linha_7'], dados['data_origem_da_fatura'], dados['descricao_da_transacao'], dados['nome_fantasia_do_estabelecimento'], dados['complemento_da_transacao'], dados['total_de_transacoes_creditadas'], dados['total_de_transacoes_debitadas'], dados['filler_7'], dados['id_operacao'])
        self.execute(sql)

    def insert_detalhe_22(self, dados):
        sql = "INSERT INTO detalhe_22 " \
                "VALUES ({}, {}, {}, '{}', {});".format(dados['numero_da_linha_8'], dados['total_de_transacoes_creditadas_nacionalmente'], dados['total_de_transacoes_debitadas_nacionalmente'], dados['filler_8'], dados['id_operacao'])
        self.execute(sql)

    def insert_detalhe_23(self, dados):
        sql = "INSERT INTO detalhe_23 " \
                "VALUES ({}, '{}', {});".format(dados['numero_da_linha_9'], dados['mensagem_extrato'], dados['id_operacao'])
        self.execute(sql)

    def insert_detalhe_24(self, dados):
        sql = "INSERT INTO detalhe_24 " \
                "VALUES({}, '{}', {});".format(dados['numero_da_linha_10'], dados['nome_da_campanha'], dados['id_operacao'])
        self.execute(sql)

    def insert_detalhe_25(self, dados):
        sql = 'INSERT INTO detalhe_25 " \
                "VALUES({}, {}, {}, {}, {}, {}, {}, {}, "{}", {});'.format(dados['numero_da_linha_11'], dados['quantidade_de_parcelas'], dados['taxa_juros'], dados['custo_efetivo_total'], dados['valor_do_iof'], dados['valor_da_taxa_de_abertura_de_credito'], dados['valor_da_parcela'], dados['valor_total_do_refinanciamento'], dados['filler_9'], dados['id_operacao'])
        self.execute(sql)

    def insert_detalhe_41(self, dados):
        sql = "INSERT INTO detalhe_41 " \
                "VALUES({}, {}, {}, {}, '{}', {}, {});".format(dados['numero_da_linha_12'], dados['data_origem'], dados['numero_de_sorte_1'], dados['serie_da_campanha'], dados['descricao'], dados['contas_debitos_recorrentes'], dados['id_operacao'])
        self.execute(sql)

    def insert_detalhe_42(self, dados):
        sql = "INSERT INTO detalhe_42 " \
                "VALUES ({}, '{}', {});".format(dados['numero_da_linha_13'], dados['filler_10'], dados['id_operacao'])
        self.execute(sql)

    def insert_detalhe_66(self, dados):
        sql = "INSERT INTO detalhe_66 " \
                "VALUES({}, '{}', '{}', '{}', '{}', {}, {});".format(dados['numero_da_linha_14'], dados['data_da_transacao'], dados['nome_da_promocao'], dados['serie'], dados['numero_de_sorte_2'], dados['data_do_sorteio'], dados['id_operacao'])
        self.execute(sql)

    def delete_past_information(self):
        print('Excluindo dados anteriores - log_error')
        sql = 'DELETE FROM log_error;'
        self.execute(sql)

        print('Excluindo dados anteriores - header_00')
        sql = 'DELETE FROM header_00;'
        self.execute(sql)

        print('Excluindo dados anteriores - detalhe_10')
        sql = 'DELETE FROM detalhe_10;'
        self.execute(sql)

        print('Excluindo dados anteriores - detalhe_11')
        sql = 'DELETE FROM detalhe_11;'
        self.execute(sql)

        print('Excluindo dados anteriores - detalhe_fatura_12')
        sql = 'DELETE FROM detalhe_fatura_12;'
        self.execute(sql)

        print('Excluindo dados anteriores - detalhe_credenciado_13')
        sql = 'DELETE FROM detalhe_credenciado_13;'
        self.execute(sql)

        print('Excluindo dados anteriores - detalhe_fatura_fisica_20')
        sql = 'DELETE FROM detalhe_fatura_fisica_20;'
        self.execute(sql)

        print('Excluindo dados anteriores - detalhe_21')
        sql = 'DELETE FROM detalhe_21;'
        self.execute(sql)

        print('Excluindo dados anteriores - detalhe_22')
        sql = 'DELETE FROM detalhe_22;'
        self.execute(sql)

        print('Excluindo dados anteriores - detalhe_23')
        sql = 'DELETE FROM detalhe_23;'
        self.execute(sql)

        print('Excluindo dados anteriores - detalhe_24')
        sql = 'DELETE FROM detalhe_24;'
        self.execute(sql)

        print('Excluindo dados anteriores - detalhe_25')
        sql = 'DELETE FROM detalhe_25;'
        self.execute(sql)

        print('Excluindo dados anteriores - detalhe_41')
        sql = 'DELETE FROM detalhe_41;'
        self.execute(sql)

        print('Excluindo dados anteriores - detalhe_42')
        sql = 'DELETE FROM detalhe_42;'
        self.execute(sql)

        print('Excluindo dados anteriores - detalhe_66')
        sql = 'DELETE FROM detalhe_66;'
        self.execute(sql)


        print('Verificando exclusão dos dados anteriores')
        sql = '''
            DELETE FROM log_error;
            DELETE FROM header_00;
            DELETE FROM detalhe_10;
            DELETE FROM detalhe_11;
            DELETE FROM detalhe_fatura_12;
            DELETE FROM detalhe_credenciado_13;
            DELETE FROM detalhe_fatura_fisica_20;
            DELETE FROM detalhe_21;
            DELETE FROM detalhe_22;
            DELETE FROM detalhe_23;
            DELETE FROM detalhe_24;
            DELETE FROM detalhe_25;
            DELETE FROM detalhe_41;
            DELETE FROM detalhe_42;
            DELETE FROM detalhe_66;
        '''

        print('Dados anteriores excluidos com sucesso')

    def get_client(self, id_operacao):
        sql = "SELECT * FROM detalhe_10 WHERE id_operacao = {};".format(id_operacao)
        self.execute_and_return(sql)

    def get_cut_date(self):
        sql = "SELECT * FROM data_corte WHERE processado = 'N';"
        return self.execute_and_return(sql)

    def update_processing(self, data, nome_arquivo):
        sql = "UPDATE data_corte " \
                "SET processado = 'S', hora_processamento = '{}', nome_arquivo = '{}' " \
                "WHERE data_corte = '{}';".format(str(datetime.now()), nome_arquivo, data)
        self.cursor.executescript(sql)
        self.conn.commit()


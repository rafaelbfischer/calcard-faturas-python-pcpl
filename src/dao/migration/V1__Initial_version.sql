CREATE TABLE log_error (
error_msg VARCHAR(500),
sql VARCHAR(500)
);

CREATE TABLE header_00 (
numero_da_linha_1 CONSTRAINT limit_numero_da_linha CHECK (numero_da_linha_1 <= 99),
nome_do_cedente VARCHAR(50),
fixo_0_1 CONSTRAINT limit_fixo_0_1 CHECK (fixo_0_1 <= 9999999999999999999),
agencia CONSTRAINT limit_agencia CHECK (agencia <= 9999),
conta_corrente CONSTRAINT limit_conta_corrente CHECK (conta_corrente <= 9999999),
data_do_documento CONSTRAINT limit_data_do_documento CHECK (data_do_documento <= 99999999),
data_do_processamento CONSTRAINT limit_data_do_processamento CHECK (data_do_processamento <= 99999999),
fixo_0_2 CONSTRAINT limit_fixo_0_2 CHECK (fixo_0_2 <= 999999),
filler1 VARCHAR(651)
);

CREATE TABLE detalhe_10 (
numero_da_linha_2 CONSTRAINT limit_numero_da_linha_2 CHECK (numero_da_linha_2 <= 99),
numero_do_cartao_do_cliente VARCHAR(20),
nome_do_cliente VARCHAR(50),
endereco_do_cliente VARCHAR(100),
numero_do_endereco VARCHAR(5),
complemento_do_endereco VARCHAR(30),
bairro_do_cliente VARCHAR(80),
cidade_do_cliente VARCHAR(60),
unidade_federativa_do_cliente VARCHAR(2),
cep_do_cliente CONSTRAINT limit_cep_do_cliente CHECK (cep_do_cliente <= 99999999),
numero_do_documento_do_cliente CONSTRAINT limit_numero_do_documento_do_cliente CHECK (numero_do_documento_do_cliente <= 9999999999),
numero_do_nosso_numero CONSTRAINT limit_numero_do_nosso_numero CHECK (numero_do_nosso_numero <= 9999999999999999999),
especie_do_documento VARCHAR(2),
aceite VARCHAR(1),
brancos_1 CONSTRAINT limit_brancos_1 CHECK (brancos_1 <= 9999999999),
numero_da_carteira CONSTRAINT limit_numero_da_carteira CHECK (numero_da_carteira <= 999),
brancos_2 CONSTRAINT limit_brancos_2 CHECK (brancos_2 <= 99999),
fixo_0 CONSTRAINT limit_fixo_0 CHECK (fixo_0 <= 99999999999999),
vencimento_da_fatura_1 CONSTRAINT limit_vencimento_da_fatura_1 CHECK (vencimento_da_fatura_1 <= 99999999),
descricoes_da_fatura_2 VARCHAR(70),
descricoes_da_fatura_3 VARCHAR(70),
descricoes_da_fatura_4 VARCHAR(70),
descricoes_da_fatura_5 VARCHAR(70),
codigo_de_barras_1 CONSTRAINT limit_codigo_de_barras_1 CHECK (codigo_de_barras_1 <= 9999999999),
deficiente CONSTRAINT limit_deficiente CHECK (deficiente <= 9),
codigo_cedente_do_cliente CONSTRAINT limit_codigo_cedente_do_cliente CHECK (codigo_cedente_do_cliente <= 9999999999),
recebimento_fatura VARCHAR(1),
email VARCHAR(50),
filler_2 CONSTRAINT limit_deficiente CHECK (deficiente <= 999999999999999999),
id_operacao INTEGER PRIMARY KEY
);

CREATE TABLE detalhe_11 (
numero_da_linha_3 CONSTRAINT limit_numero_da_linha_3 CHECK (numero_da_linha_3 <= 99),
limite_maximo_para_compras_nacional_1 CONSTRAINT limit_limite_maximo_para_compras_nacional_1 CHECK (limite_maximo_para_compras_nacional_1 <= 99),
limite_maximo_para_saque_nacional_2 CONSTRAINT limit_limite_maximo_para_saque_nacional_2 CHECK (limite_maximo_para_saque_nacional_2 <= 99999999999999),
limite_maximo_para_compras_nacional_3 CONSTRAINT limit_nlimite_maximo_para_compras_nacional_3 CHECK (limite_maximo_para_compras_nacional_3 <= 99999999999999),
valores_de_encargos_financeiros CONSTRAINT limit_valores_de_encargos_financeiros CHECK (valores_de_encargos_financeiros <= 99999999999999),
juros_1 VARCHAR(6),
valores_de_encargos_de_atraso_1 CONSTRAINT limit_valores_de_encargos_de_atraso_1 CHECK (valores_de_encargos_de_atraso_1 <= 99999999999999),
juros_2 VARCHAR(6),
limite_maximo_para_parcelado CONSTRAINT limit_limite_maximo_para_parcelado CHECK (limite_maximo_para_parcelado <= 99999999999999),
limite_maximo_para_compras CONSTRAINT limit_limite_maximo_para_compras CHECK (limite_maximo_para_compras <= 99999999999999),
valores_de_encargos_de_atraso_2 CONSTRAINT limit_valores_de_encargos_de_atraso_2 CHECK (valores_de_encargos_de_atraso_2 <= 99999999999999999999999),
valor_do_custo_efetivo_total CONSTRAINT limit_valor_do_custo_efetivo_total CHECK (valor_do_custo_efetivo_total <= 99999999999999),
juros_3 VARCHAR(6),
data_do_fechamento_da_proxima_fatura CONSTRAINT limit_data_do_fechamento_da_proxima_fatura CHECK (data_do_fechamento_da_proxima_fatura <= 99999999),
total_saldo_parcelado_pendente_de_liquidacao CONSTRAINT limit_total_saldo_parcelado_pendente_de_liquidacao CHECK (total_saldo_parcelado_pendente_de_liquidacao <= 99999999999999),
total_credito_parcelado CONSTRAINT limit_total_credito_parcelado CHECK (total_credito_parcelado <= 99999999999999),
total_credito_parcela_parcelado CONSTRAINT limit_total_credito_parcela_parcelado CHECK (total_credito_parcela_parcelado <= 99999999999999),
encargos_para_o_proximo_mes_no_caso_de_pagamento_minimo_da_fatura CONSTRAINT limit_encargos_para_o_proximo_mes_no_caso_de_pagamento_minimo_da_fatura CHECK (encargos_para_o_proximo_mes_no_caso_de_pagamento_minimo_da_fatura <= 99999999999999),
filler_3 VARCHAR(581),
id_operacao INTEGER,
FOREIGN KEY (id_operacao) REFERENCES detalhe_10(id_operacao)
);

CREATE TABLE detalhe_fatura_12 (
numero_da_linha_4 CONSTRAINT limit_pontos_remanescentes CHECK (pontos_remanescentes <= 99),
saldo_anterior_do_extrato CONSTRAINT limit_saldo_anterior_do_extrato CHECK (saldo_anterior_do_extrato <= 99999999999999),
creditos_nacionais CONSTRAINT limit_creditos_nacionais CHECK (creditos_nacionais <= 99999999999999),
debitos_nacionais CONSTRAINT limit_debitos_nacionais CHECK (debitos_nacionais <= 99999999999999),
saldo_atual_do_extrato CONSTRAINT limit_saldo_atual_do_extrato CHECK (saldo_atual_do_extrato <= 99999999999999),
valor_minimo_do_extrato CONSTRAINT limit_valor_minimo_do_extrato CHECK (valor_minimo_do_extrato <= 99999999999999),
pagamentos CONSTRAINT limit_npagamentos CHECK (pagamentos <= 99999999999999),
valor_das_compras_nacionais CONSTRAINT limit_valor_das_compras_nacionais CHECK (valor_das_compras_nacionais <= 99999999999999),
encargos_de_financiamento CONSTRAINT limit_encargos_de_financiamento CHECK (encargos_de_financiamento <= 99999999999999),
acertos_diversos CONSTRAINT limit_nacertos_diversos CHECK (acertos_diversos <= 99999999999999),
quantidade_de_dias_em_atraso_1 CONSTRAINT limit_quantidade_de_dias_em_atraso_1 CHECK (quantidade_de_dias_em_atraso_1 <= 99999999999999),
pontuacao_total_no_ciclo CONSTRAINT limit_pontuacao_total_no_ciclo CHECK (pontuacao_total_no_ciclo <= 99999999999999),
pontuacao_usada_no_ciclo CONSTRAINT limit_pontuacao_usada_no_ciclo CHECK (pontuacao_usada_no_ciclo <= 99999999999999),
pontos_remanescentes CONSTRAINT limit_pontos_remanescentes CHECK (pontos_remanescentes <= 99999999999999),
senha VARCHAR(5),
aniversariante_do_mes VARCHAR(1),
codigo_de_barras_2 VARCHAR(44),
linha_digitavel VARCHAR(80),
emite_extrato CONSTRAINT limit_emite_extrato CHECK (emite_extrato <= 1),
filler_4 VARCHAR(440),
id_operacao INTEGER,
FOREIGN KEY (id_operacao) REFERENCES detalhe_10(id_operacao)
);

CREATE TABLE detalhe_credenciado_13 (
numero_da_linha_5 CONSTRAINT limit_numero_da_linha_5 CHECK (numero_da_linha_5 <= 99),
codigo_da_origem_comercial CONSTRAINT limit_codigo_da_origem_comercial CHECK (codigo_da_origem_comercial <= 999999),
cnpj_do_estabelecimento CONSTRAINT limit_ccnpj_do_estabelecimento CHECK (cnpj_do_estabelecimento <= 99999999999999),
nome_fantasia_do_lojista VARCHAR(20),
codigo_da_fantasia_basica CONSTRAINT limit_codigo_da_fantasia_basica CHECK (codigo_da_fantasia_basica <= 999),
quantidade_de_dias_em_atraso_2 CONSTRAINT limit_quantidade_de_dias_em_atraso_2 CHECK (quantidade_de_dias_em_atraso_2 <= 99999),
codigo_do_produto CONSTRAINT limit_codigo_do_produto CHECK (codigo_do_produto <= 9999999),
descricao_do_produto VARCHAR(80),
filler_5 VARCHAR(602),
id_operacao INTEGER,
FOREIGN KEY (id_operacao) REFERENCES detalhe_10(id_operacao)
);

CREATE TABLE detalhe_fatura_fisica_20 (
numero_da_linha_6 CONSTRAINT limit_numero_da_linha_6 CHECK (numero_da_linha_6 <= 99),
nome_do_plastico_do_cartao VARCHAR(50),
filler_6 VARCHAR(702),
id_operacao INTEGER,
FOREIGN KEY (id_operacao) REFERENCES detalhe_10(id_operacao)
);

CREATE TABLE detalhe_21 (
numero_da_linha_7 CONSTRAINT limit_numero_da_linha_7 CHECK (numero_da_linha_7 <= 99),
data_origem_da_fatura CONSTRAINT limit_data_origem_da_fatura CHECK (data_origem_da_fatura <= 99999999),
descricao_da_transacao VARCHAR(40),
nome_fantasia_do_estabelecimento VARCHAR(25),
complemento_da_transacao VARCHAR(11),
total_de_transacoes_creditadas CONSTRAINT limit_total_de_transacoes_creditadas CHECK (total_de_transacoes_creditadas <= 99999999999999),
total_de_transacoes_debitadas CONSTRAINT limit_total_de_transacoes_debitadas CHECK (total_de_transacoes_debitadas <= 99999999999999),
filler_7 VARCHAR(654),
id_operacao INTEGER,
FOREIGN KEY (id_operacao) REFERENCES detalhe_10(id_operacao)
);

CREATE TABLE detalhe_22 (
numero_da_linha_8 CONSTRAINT limit_numero_da_linha_7 CHECK (numero_da_linha_8 <= 99),
total_de_transacoes_creditadas_nacionalmente CONSTRAINT limit_total_de_transacoes_creditadas_nacionalmente CHECK (total_de_transacoes_creditadas_nacionalmente <= 99999999999999),
total_de_transacoes_debitadas_nacionalmente CONSTRAINT limit_total_de_transacoes_debitadas_nacionalmente CHECK (total_de_transacoes_debitadas_nacionalmente <= 99999999999999),
filler_8 VARCHAR(724),
id_operacao INTEGER,
FOREIGN KEY (id_operacao) REFERENCES detalhe_10(id_operacao)
);

CREATE TABLE detalhe_23 (
numero_da_linha_9 CONSTRAINT limit_numero_da_linha_9 CHECK (numero_da_linha_9 <= 99),
mensagem_extrato VARCHAR(79),
id_operacao INTEGER,
FOREIGN KEY (id_operacao) REFERENCES detalhe_10(id_operacao)
);

CREATE TABLE detalhe_24 (
numero_da_linha_10 CONSTRAINT limit_numero_da_linha_10 CHECK (numero_da_linha_10 <= 99),
nome_da_campanha VARCHAR(50),
id_operacao INTEGER,
FOREIGN KEY (id_operacao) REFERENCES detalhe_10(id_operacao)
);

CREATE TABLE detalhe_25 (
numero_da_linha_11 CONSTRAINT limit_numero_da_linha_11 CHECK (numero_da_linha_11 <= 99),
quantidade_de_parcelas CONSTRAINT limit_quantidade_de_parcelas CHECK (quantidade_de_parcelas <= 9999),
taxa_juros CONSTRAINT limit_taxa_juros CHECK (taxa_juros <= 99999999999999),
custo_efetivo_total CONSTRAINT limit_custo_efetivo_total CHECK (custo_efetivo_total <= 99999999999999),
valor_do_iof CONSTRAINT limit_valor_do_iof CHECK (valor_do_iof <= 99999999999999),
valor_da_taxa_de_abertura_de_credito CONSTRAINT limit_valor_da_taxa_de_abertura_de_credito CHECK (valor_da_taxa_de_abertura_de_credito <= 99999999999999),
valor_da_parcela CONSTRAINT limit_valor_da_parcela CHECK (valor_da_parcela <= 99999999999999),
valor_total_do_refinanciamento CONSTRAINT limit_valor_total_do_refinanciamento CHECK (valor_total_do_refinanciamento <= 99999999999999),
filler_9 VARCHAR(2),
id_operacao INTEGER,
FOREIGN KEY (id_operacao) REFERENCES detalhe_10(id_operacao)
);

CREATE TABLE detalhe_41 (
numero_da_linha_12 CONSTRAINT limit_numero_da_linha_12 CHECK (numero_da_linha_12 <= 99),
data_origem CONSTRAINT limit_data_origem CHECK (data_origem <= 99999999),
numero_de_sorte_1 CONSTRAINT limit_numero_de_sorte_1 CHECK (numero_de_sorte_1 <= 9999999999),
serie_da_campanha CONSTRAINT limit_serie_da_campanha CHECK (serie_da_campanha <= 999),
descricao VARCHAR(40),
contas_debitos_recorrentes CONSTRAINT limit_contas_debitos_recorrentes CHECK (contas_debitos_recorrentes <= 9999999999),
id_operacao INTEGER,
FOREIGN KEY (id_operacao) REFERENCES detalhe_10(id_operacao)
);

CREATE TABLE detalhe_42 (
numero_da_linha_13 CONSTRAINT limit_numero_da_linha_13 CHECK (numero_da_linha_13 <= 99),
filler_10 VARCHAR(100),
id_operacao INTEGER,
FOREIGN KEY (id_operacao) REFERENCES detalhe_10(id_operacao)
);

CREATE TABLE detalhe_66 (
numero_da_linha_14 CONSTRAINT limit_numero_da_linha_14 CHECK (numero_da_linha_14 <= 99),
data_da_transacao VARCHAR(8) CONSTRAINT limit_data_da_transacao CHECK (data_da_transacao <= 99999999),
nome_da_promocao VARCHAR(74),
serie CONSTRAINT limit_serie CHECK (serie <= 999),
numero_de_sorte_2 CONSTRAINT limit_numero_de_sorte_2 CHECK (numero_de_sorte_2 <= 99999),
data_do_sorteio VARCHAR(8) CONSTRAINT limit_data_do_sorteio CHECK (data_do_sorteio <= 99999999),
id_operacao INTEGER,
FOREIGN KEY (id_operacao) REFERENCES detalhe_10(id_operacao)
);

CREATE TABLE data_corte (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	data_corte VARCHAR(10),
	processado VARCHAR(1),
	nome_arquivo TEXT,
	hora_processamento DATETIME
);

INSERT INTO data_corte (data_corte, processado) VALUES
('2019-01-18', 'N'),
('2019-01-24', 'N'),
('2019-01-29', 'N'),
('2019-01-31', 'N'),
('2019-02-08', 'N'),
('2019-02-14', 'N'),
('2019-02-15', 'N'),
('2019-02-21', 'N'),
('2019-02-26', 'N'),
('2019-03-01', 'N'),
('2019-03-08', 'N'),
('2019-03-13', 'N'),
('2019-03-20', 'N'),
('2019-03-22', 'N'),
('2019-03-28', 'N'),
('2019-04-01', 'N'),
('2019-04-09', 'N'),
('2019-04-12', 'N'),
('2019-04-18', 'N'),
('2019-04-23', 'N'),
('2019-04-26', 'N'),
('2019-05-01', 'N'),
('2019-05-09', 'N'),
('2019-05-14', 'N'),
('2019-05-21', 'N'),
('2019-05-24', 'N'),
('2019-05-29', 'N'),
('2019-05-31', 'N'),
('2019-06-07', 'N'),
('2019-06-13', 'N');

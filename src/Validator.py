import os
from src.CheckFile import CheckFile
from src.dao.ScriptSql import ScriptSql
#from src.sftp.Sftp import Sftp
from src.utils.TmpFile import TmpFile
from src.utils.Utils import Utils


def validator():

    sql = ScriptSql()
    utils = Utils()
    initial_time = utils.get_actual_time()
    sql.delete_past_information()
    #print('Conectando no FTP')
    #sftp = Sftp()          #CONECTA NO FTP
    #sftp.get_file_cdt()    #PEGA O ARQUIVO
    qnt_line = ''
    file_zip = True

    if file_zip:
        for _, _, files in os.walk('arquivos_validacao/'):
            for file in files:
                if '.txt' in file:
                    try:
                        file2 = 'arquivos_validacao/' + file
                        print('Abrindo arquivo para chegagem')
                        print(file)
                        invoice_file = open(file2, "r", encoding="ISO-8859-1")
                        print('Arquivo aberto')

                        validator = CheckFile()
                        qnt_line = validator.executar(invoice_file, sql)

                        invoice_file.close()
                        tmp_file = TmpFile()

                        print('Escrevendo o arquivo temporário')
                        tmp_file.write_sql_tmp_file(sql)
                        print('Finalizado a escrita do arquivo temporário')

                        final_time = utils.get_final_time(initial_time)
                        if not sql.get_log():
                            print(final_time)
                            os.remove(file2)
                        else:
                            #utils.send_log_mail(qnt_line, final_time)
                            return False

                    except Exception as exception:
                        sql.insert_log_error_and_commit('ERROR', str(exception))
                        #utils.send_log_mail()
                        print(exception)
                        return False

            final_time = utils.get_final_time(initial_time)
            print(final_time)
            print('Script finalizado')
            #utils.send_log_mail(qnt_line, final_time)  ' ENVIAR EMAIL
            return True
    return False

# encoding: utf-8
from src.dao.ScriptSql import ScriptSql
from src.layout_arquivo_fatura.Layout import HEADER, DETALHE_1, DETALHE_2, DETALHE_FATURA, DETALHE_CREDENCIADOS, \
    DETALHE_FATURA_FISICA, \
    DETALHE_6, \
    DETALHE_7, DETALHE_8, DETALHE_9, DETALHE_10, DETALHE_11, DETALHE_12, DETALHE_13


class CheckFile(object):


    def quantidade_faturas(self, arquivo):
        quantidade_fatura = 0
        for linha in arquivo.readlines():
            if linha[0:2] == "10":
                quantidade_fatura += 1
            elif linha[0:2] == "00":
                print("TEM HEADER")
        return quantidade_fatura

    def quantidade_fatura_conductor(self, quantidade_faturas_arquivo):
        quantidade = quantidade_faturas_arquivo.readlines()
        a = quantidade[1].replace('Total de Registros: ', '').replace('\n', '')
        return int(a)

    def coluna(self, linha, layout):
        posicao = 0
        dados = {}
        for k, v in layout.items():
            frase = linha[posicao:posicao + v]
            dados[k] = frase.replace('\n', '').replace("'", " ").strip()
            if not dados[k] and k != 'filler_9':
                dados[k] = None

            posicao += v
        return dados

    def executar(self, arquivo_faturas, sql):
        #sql = ScriptSql()
        id_operacao = {}
        qnt = 0
        qntRegistros = 0

        #sql_dados = ''
        # #VERIFICAR SE EXISTE POSSIBILIDADE DE CONCATENAR OS SQL E EXECUTAR REGISTROS/REGISTROS _
        #   E NAO LINHA/LINHA - _
        #   NA TEORIA DIMINUIRIA 16X O TEMPO DE EXECUCAO, _
        #   1600% A MENOS

        for linha in arquivo_faturas.readlines():
            if linha[0:2] == "00":
                dados = self.coluna(linha, HEADER)
                sql.insert_header_fatura(dados)

            elif linha[0:2] == "10":
                dados = self.coluna(linha, DETALHE_1)
                id_operacao = {'id_operacao': str(linha[422:430] + linha[710:720])}
                dados.update(id_operacao)
                sql.insert_detalhe_10(dados)

                qntRegistros += 1
                print('Linha: ' + str(qntRegistros))

            elif linha[0:2] == "11":
                dados = self.coluna(linha, DETALHE_2)
                dados.update(id_operacao)
                sql.insert_detalhe_11(dados)

            elif linha[0:2] == "12":
                dados = self.coluna(linha, DETALHE_FATURA)
                dados.update(id_operacao)
                if dados['emite_extrato'] == "0":
                    dados['codigo_de_barras_2'] = "0000000000"
                    sql.insert_detalhe_fatura_12_nao_extratavel(dados)
                else:
                    sql.insert_detalhe_fatura_12(dados)

            elif linha[0:2] == "13":
                dados = self.coluna(linha, DETALHE_CREDENCIADOS)
                dados.update(id_operacao)
                sql.insert_detalhe_credenciado_13(dados)

            elif linha[0:2] == "20":
                dados = self.coluna(linha, DETALHE_FATURA_FISICA)
                dados.update(id_operacao)
                sql.insert_detalhe_detalhe_fatura_fisica_20(dados)

            elif linha[0:2] == "21":
                dados = self.coluna(linha, DETALHE_6)
                dados.update(id_operacao)
                sql.insert_detalhe_21(dados)

            elif linha[0:2] == "22":
                dados = self.coluna(linha, DETALHE_7)
                dados.update(id_operacao)
                sql.insert_detalhe_22(dados)

            elif linha[0:2] == "23":
                dados = self.coluna(linha, DETALHE_8)
                dados.update(id_operacao)
                sql.insert_detalhe_23(dados)

            elif linha[0:2] == "24":
                dados = self.coluna(linha, DETALHE_9)
                dados.update(id_operacao)
                sql.insert_detalhe_24(dados)

            elif linha[0:2] == "25":
                dados = self.coluna(linha, DETALHE_10)
                dados.update(id_operacao)
                sql.insert_detalhe_25(dados)

            elif linha[0:2] == "41":
                dados = self.coluna(linha, DETALHE_11)
                dados.update(id_operacao)
                sql.insert_detalhe_41(dados)

            elif linha[0:2] == "42":
                dados = self.coluna(linha, DETALHE_12)
                dados.update(id_operacao)
                sql.insert_detalhe_42(dados)

            elif linha[0:2] == "66":
                dados = self.coluna(linha, DETALHE_13)
                dados.update(id_operacao)
                sql.insert_detalhe_66(dados)

            else:
                sql.insert_log_error_and_commit("ERRO AO OBTER TIPO DE LAYOUT", linha[0:2])

            qnt += 1
            #print(qnt)
        return qnt

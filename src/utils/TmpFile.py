# encoding: utf-8


class TmpFile(object):

    data = []

    def __init__(self):
        pass

    def set_data_sql(self, sql):
        self.data.append(sql + '\n')

    def get_data_sql(self):
        return self.data

    def write_sql_tmp_file(self, sql):
        #from src.dao.ScriptSql import ScriptSql
        #sql = ScriptSql()
        sql.execute_arquivo(self.get_data_sql())

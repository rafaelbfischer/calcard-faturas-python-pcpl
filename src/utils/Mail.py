# encoding: utf-8
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders


class Mail(object):
    sender = 'testecalcard@bol.com.br'
    password = 'calc@2018'
    addressee = 'francisco.franca@terceiros.calcard.com.br'

    def send_config(self, msg_root):
        try:
            server = smtplib.SMTP("smtps.bol.com.br", 587)
            server.login(self.sender, self.password)
            server.sendmail(self.sender, self.addressee, msg_root.as_string())
            server.quit()
        except Exception as e:
            print(e)

    def send_mail(self, log, file_attachment=None):
        try:
            msg_root = MIMEMultipart()
            msg_root['Subject'] = 'LOG VALIDAÇÃO EMAIL FATURAS'
            msg_root['From'] = self.sender
            msg_root['To'] = self.addressee
            body = log
            msg_root.attach(MIMEText(body, 'plain'))
            if file_attachment:
                attachment = open(file_attachment, "rb")
                part = MIMEBase('application', 'octet-stream')
                part.set_payload((attachment).read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', "attachment; filename= log_erro.csv")

                msg_root.attach(part)

            self.send_config(msg_root)
        except Exception as e:
            print(e)

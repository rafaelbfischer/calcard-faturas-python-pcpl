# encoding: utf-8
import os
import zipfile
from src.dao.ScriptSql import ScriptSql
from src.utils.TmpFile import TmpFile
from src.utils.Mail import Mail
from datetime import datetime
import csv


class Utils(object):

    def delete_file(self, absolute_file_path):
        os.remove(absolute_file_path)

    def delete_all_files(self, path):
        try:
            tmp_file = TmpFile()
            tmp_file.close_tmp_files()
        except Exception:
            pass
        for _, _, files in os.walk(path):
            for file in files:
                os.remove(path + '/' + file)

    def extract_file_zip(self, zip_file):
        local_file = 'arquivos_validacao'
        zip_file_path = zipfile.ZipFile(zip_file)
        zip_file_path.extractall(local_file)
        zip_file_path.close()

        for _, _, file in os.walk(local_file):
            return local_file + '/' + file[0]

    def send_log_mail(self, number_line=None, time=None):
            mail = Mail()
            sql = ScriptSql()
            log = sql.get_log()
            if not log:
                msg = 'Não existe log de erro para essa operação! Total de Linhas Analisadas: {} ! Levou: {}'.format(number_line, time)
                mail.send_mail(msg)
            else:
                msg = 'Encontramos erro ao processar o arquivo! Total de Linhas Analisadas: {} ! Levou: {}'.format(number_line, time)
                file_name = self.save_log_csv(log)
                mail.send_mail(msg, file_name)

    def save_log_csv(self, log):
        name_file = "arquivo_log_erro/log_erro.csv"
        with open(name_file, 'w') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=';')
            spamwriter.writerow(['Erro', 'Quantidade'])
            for log in log:
                spamwriter.writerow(log)
        return name_file

    def get_actual_time(self):
        return f"{datetime.now():%Y/%m/%d %H:%M:%S}"

    def get_final_time(self, initial_time):
        final_time = self.get_actual_time()
        format = '%Y/%m/%d %H:%M:%S'
        return (datetime.strptime(final_time, format) - datetime.strptime(initial_time, format))


import paramiko
from src.utils.Mail import Mail
from src.utils.Utils import Utils


class Sftp(object):

    def get_file_cdt(self):
        k = "src/sftp/conductor.pem"
        hostname = 'sftp.conductor.com.br'
        username = 'gabriela'
        absolute_file_path = 'arquivos_validacao/'

        print('Carregando arquivo de conexão PEM do FTP')
        key = paramiko.DSSKey.from_private_key_file(k)
        self.client = paramiko.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.connect(hostname=hostname, username=username, pkey=key)

        print('Abrindo FTP')
        sftp = self.client.open_sftp()

        print('Mudando para a pasta DaProducao')
        sftp.chdir('/DaProducao/')

        file_cdt = None

        print('Comando ListDir')
        if sftp.listdir():

            print('Varre os arquivos da pasta')
            for file in sftp.listdir():
                print(file)
                if file.startswith('CALC'):
                    sftp.get(file, absolute_file_path + file)
                    file_cdt = True

        print('Fecha a conexão FTP')
        sftp.close()
        print('Fecha o FTP')
        self.client.close()

        if not file_cdt:
            mail = Mail()
            mail.send_mail("NÃO EXISTE ARQUIVO NO SFTP!!!")
            print("NÃO EXISTE ARQUIVO NO SFTP!!!")
            return False

        utils = Utils()
        utils.extract_file_zip(absolute_file_path + file)
        return file

    def send_file_sftp(self, file):
        path = 'arquivos_validacao/'
        key = paramiko.RSAKey.from_private_key_file("src/sftp/salesforcecdt.pem")
        self.client = paramiko.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.connect(hostname='ec2-52-0-10-97.compute-1.amazonaws.com', username='centos', pkey=key)

        sftp = self.client.open_sftp()
        sftp.put(path + file, '/home/centos/WORKINGDIR/' + file)

        sftp.close()
        self.client.close()

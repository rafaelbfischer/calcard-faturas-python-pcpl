# encoding: utf-8

#COD - 00
HEADER = {
    'numero_da_linha_1': 2,
    'nome_do_cedente': 50,
    'fixo_0_1': 19,
    'agencia': 4,
    'conta_corrente': 7,
    'data_do_documento': 8,
    'data_do_processamento': 8,
    'fixo_0_2': 6,
    'filler_1': 651
}

#COD - 10
DETALHE_1 = {
    'numero_da_linha_2': 2,
    'numero_do_cartao_do_cliente': 20,
    'nome_do_cliente': 50,
    'endereco_do_cliente_1': 100,
    'numero_do_endereco': 6,
    'complemento_do_endereco': 30,
    'bairro_do_cliente': 80,
    'cidade_do_cliente': 60,
    'unidade_federativa_do_cliente': 2,
    'cep_do_cliente': 8,
    'numero_do_documento_do_cliente': 10,
    'numero_do_nosso_numero': 19,
    'especie_do_documento': 2,
    'aceite': 1,
    'brancos_1': 10,
    'numero_da_carteira': 3,
    'brancos_2': 5,
    'fixo_0': 14,
    'vencimento_da_fatura': 8,
    'descricoes_da_fatura_1': 70,
    'descricoes_da_fatura_2': 70,
    'descricoes_da_fatura_3': 70,
    'descricoes_da_fatura_4': 70,
    'codigo_de_barras_1': 10,
    'deficiente': 1,
    'codigo_cedente_do_cliente': 10,
    'recebmento_fatura': 1,
    'email': 50,
    'filler': 18
}

#COD - 11
DETALHE_2 = {
    'numero_da_linha_3': 2,
    'limite_maximo_para_compras_nacional_1': 14,
    'limite_maximo_para_saque_nacional_2': 14,
    'limite_maximo_para_compras_nacional_3': 14,
    'valores_de_encargos_financeiros': 14,
    'juros_1': 6,
    'valores_de_encargos_de_atraso_1': 14,
    'juros_2': 6,
    'limite_maximo_para_parcelado': 14,
    'limite_maximo_para_compras': 14,
    'valores_de_encargos_de_atraso_2': 23,
    'valor_do_custo_efetivo_total': 14,
    'juros_3': 6,
    'data_do_fechamento_da_proxima_fatura': 8,
    'total_saldo_parcelado_pendente_de_liquidacao': 14,
    'total_credito_parcelado': 14,
    'total_credito_parcela_parcelado': 14,
    'encargos_para_o_proximo_mes_no_caso_de_pagamento_minimo_da_fatura': 14,
    'filler_3': 581
}

#COD - 12
DETALHE_FATURA = {
    'numero_da_linha_4': 2,
    'saldo_anterior_do_extrato': 14,
    'creditos_nacionais': 14,
    'debitos_nacionais': 14,
    'saldo_atual_do_extrato': 14,
    'valor_minimo_do_extrato': 14,
    'pagamentos': 14,
    'valor_das_compras_nacionais': 14,
    'encargos_de_financiamento': 14,
    'acertos_diversos': 14,
    'quantidade_de_dias_em_atraso_1': 14,
    'pontuacao_total_no_ciclo': 14,
    'pontuacao_usada_no_ciclo': 14,
    'pontos_remanescentes': 14,
    'senha': 5,
    'aniversariante_do_mes': 1,
    'codigo_de_barras_2': 44,
    'linha_digitavel': 80,
    'emite_extrato': 1, #1- emite | 0 - não emite
    'filler_4': 440
}

#COD - 13
DETALHE_CREDENCIADOS = {
    'numero_da_linha_5': 2,
    'codigo_da_origem_comercial': 6,
    'cnpj_do_estabelecimento': 14,
    'nome_fantasia_do_lojista': 20,
    'codigo_da_fantasia_basica': 3,
    'quantidade_de_dias_em_atraso_2': 5,
    'codigo_do_produto': 7,
    'descricao_do_produto': 80,
    'filler_5': 602
}

#COD - 20
DETALHE_FATURA_FISICA = {
    'numero_da_linha_6': 2,
    'nome_do_plastico_do_cartao': 50,
    'filler_6': 702
}

#COD - 21
DETALHE_6 = {
    'numero_da_linha_7': 2,
    'data_origem_da_fatura': 8,
    'descricao_da_transacao': 40,
    'nome_fantasia_do_estabelecimento': 25,
    'complemento_da_transacao': 11,
    'total_de_transacoes_creditadas': 14,
    'total_de_transacoes_debitadas': 14,
    'filler_7': 654
}

#COD - 22
DETALHE_7 = {
    'numero_da_linha_8': 2,
    'total_de_transacoes_creditadas_nacionalmente': 14,
    'total_de_transacoes_debitadas_nacionalmente': 14,
    'filler_8': 724
}

#COD - 23
DETALHE_8 = {
    'numero_da_linha_9': 2,
    'mensagem_extrato': 79
}

#COD - 24
DETALHE_9 = {
    'numero_da_linha_10': 2,
    'nome_da_campanha': 50
}

#COD - 25
DETALHE_10 = {
    'numero_da_linha_11': 2,
    'quantidade_de_parcelas': 4,
    'taxa_juros': 14,
    'custo_efetivo_total': 14,
    'valor_do_iof': 14,
    'valor_da_taxa_de_abertura_de_credito': 14,
    'valor_da_parcela': 14,
    'valor_total_do_refinanciamento': 14,
    'filler_9': 2
}

#COD - 41
DETALHE_11 = {
    'numero_da_linha_12': 2,
    'data_origem': 8,
    'numero_de_sorte_1': 10,
    'serie_da_campanha': 3,
    'descricao': 40,
    'contas_debitos_recorrentes': 10
}

#COD - 42
DETALHE_12 = {
    'numero_da_linha_13': 2,
    'filler_10': 100
}

#COD - 66
DETALHE_13 = {
    'numero_da_linha_14': 2,
    'data_da_transacao': 8,
    'nome_da_promocao': 74,
    'serie': 3,
    'numero_de_sorte_2': 5,
    'data_do_sorteio': 8
}